/*
 * C Code for demostration of void pointer.
 * Written by: Saurabh Namjoshi
 * Date: 15/11/2017                
 * */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    void *ptr;
    int i =100;
    float f = 23.3;
    char c = 'a';

    ptr = &i;
    printf("ptr = %d\n",*((int*)ptr));


    ptr = &f;
    printf("ptr = %f\n",*((float*)ptr));
    
    ptr = &c;
    printf("ptr = %c\n",*((char*)ptr));
   
   return 0; 
    
}
