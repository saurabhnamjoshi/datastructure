/*
 * C Code for demostration of void pointer.
 * Written by: Saurabh Namjoshi
 * Date: 20/11/2017                
 * */

#include <stdio.h>
#include <stdlib.h>

int compare(void *ptr1, void *ptr2);

void *larger (void *dataPtr1, void *dataPtr2, int(*ptrToCmpFun)(void*, void*))
{
    if((*ptrToCmpFun)(dataPtr1, dataPtr2) > 0)
        return dataPtr1;
    else
        return dataPtr2;
}


int main(void)
{
    int i =100;
    int j = 200;
    float f = 23.3;
    float g = 14.2;
    char c = 'a';
    char d = 'b';
    int i_lrg;
    float f_lrg;
    char c_lrg;

    i_lrg = (*(int*)larger(&i, &j, compare));
    printf("Int Larger value is = %d\n",i_lrg);


    f_lrg = (*(float*)larger(&f, &g, compare));
    printf("Float Larger value is = %5.1f\n",f_lrg);
    
    c_lrg = (*(char*)larger(&c, &d, compare));
    printf("char Larger value is = %c\n",c_lrg);
   
   return 0; 
    
}

int compare(void *ptr1, void *ptr2)
{
            if(*(int*)ptr1 >= *(int*)ptr2)
                return 1;
            else
                return -1;

}

