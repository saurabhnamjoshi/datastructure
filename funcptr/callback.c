#include<stdio.h>
void B(void (*Ptr)())
{
    printf("B\n");
    (*Ptr)();
}
void A()
{
    printf("A");
}
int main(void)
{
    void (*Ptr)();
    Ptr=&A;
    B(Ptr);
    return 0;
}
