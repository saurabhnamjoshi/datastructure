/*
 * C Code for demostration of link list.
 * Written by: Saurabh Namjoshi
 * Date: 15/11/2017                
 * */

#include <stdio.h>
#include <stdlib.h>
typedef struct node
{
    void *dataptr;
    struct node *link;
}NODE;

NODE *createNode(NODE **node, void *itemPtr);
int main(int argc, char *argv[])
{
    int *newData;
    int *nodeData;
    NODE *node = NULL,*p = NULL;
    int i;
    newData = (int*) malloc (sizeof(int));
    for(i =0 ;i<argc;i++)
        printf("arguments = %d:%s\n",i, argv[i]);
    newData = (int*) malloc (sizeof(int));
    sscanf(argv[1], "%d", newData);
    printf("%d\n",*newData);
    createNode(&node ,newData);
    for(i=2;i<= argc-1; i++)
    {
        newData = (int*) malloc (sizeof(int));
        sscanf(argv[i], "%d", newData);
        printf("%d\n",*newData);
        createNode(&node, newData);
    }
    
    i = 0;
    for(p = node; p != NULL; p = p->link)
    {
        nodeData = (int*)p->dataptr;
        printf("Data from node%d: %d\n",i++,*nodeData);
    }
    
    free(newData);
    free(node);
    return 0;
}

NODE *createNode(NODE **node, void *itemPtr)
{
   NODE *nodePtr;  
   nodePtr = (NODE*)malloc(sizeof(NODE));
   nodePtr->dataptr = itemPtr;
   nodePtr->link = *node; 
   *node = nodePtr;
   return nodePtr; 
    
}
